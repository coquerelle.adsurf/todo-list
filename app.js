var app = angular.module('todo', []);


app.directive('ngBlur', function(){
	return function(scope, elem, attrs){
		elem.bind('blur', function(){
			scope.$apply(attrs.ngBlur);
		})
	}
})



app.controller('TodoCtrl', function($scope, filterFilter, $http, $location){
	$scope.todos = [];
	
				var host = location.origin;
				var socket = io.connect(host);

				// mise a jour du cache sur le nouveau client :-/
				socket.on('cache', function(cache){
		  			$scope.todos.push(cache);
		  			$scope.$apply();
		  		});
				
				// mise a jour des ajouts
				socket.on('name', function(name){
		  			$scope.todos.push({name : name, completed : false});
		  			$scope.$apply();
		  		});
	
				// mise a jour des suppressions
				socket.on('clear', function(idx){
		  			$scope.todos.splice(idx,1);
		  			$scope.$apply();
		  		});
				
				// mise a jour état de la tache
				socket.on('completed', function(index,completed){
					todo = $scope.todos[index];
					todo.completed = completed
					$scope.todos.splice(index,1,todo);
		  			$scope.$apply();
		  		});
				
				// mise a jour état de la tache allchecked
				socket.on('allchecked', function(allchecked){
					$scope.todos.forEach(function(todo){
						todo.completed = allchecked;
					})
		  			$scope.$apply();
		  		});

	// actions filtres
	$scope.$watch('todos', function(){
		$scope.remaining = filterFilter($scope.todos, {completed:false}).length;
		$scope.allchecked = !$scope.remaining;
	}, true)

	if($location.path() == ''){ $location.path('/')}
	$scope.location = $location;
	$scope.$watch('location.path()', function(path){
		$scope.statusFilter =
			(path == '/active') ? {completed : false} :
			(path == '/done') ? {completed : true} :
			null;
	});

	// ordre de suppression
	$scope.removeTodo = function(index){
		socket.emit('clear',index)
	};
	
	// état de la tache 
	$scope.todoCompleted = function(index){
		test = $scope.todos[index]
		completed = test.completed
		socket.emit('completed',index,completed)
	};
	
	// ajout tache
	$scope.addTodo = function(){
		
			name = $scope.newtodo;
		socket.emit('name',name)
		$scope.newtodo = '';
		return false;
	};
	// rééditer tache
	$scope.editTodo = function(todo){
		$scope.todoChanged = function(index){
			socket.emit('todoEdit',index,$scope.word)
		};
		todo.editing = false;
	};
	// sélectionner toutes les taches
	$scope.checkAllTodo = function(allchecked){
		socket.emit('allchecked',allchecked)
		$scope.todos.forEach(function(todo){
			todo.completed = allchecked;
		});
	};


	
});

